package com.example.watercan.Adapter

import android.content.Context;

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.watercan.Fragment.CancelledFragment
import com.example.watercan.Fragment.DeliveredFragment
import com.example.watercan.Fragment.OrderFragment

class OrderAdapter(private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                val orderFragment: OrderFragment = OrderFragment()
                return OrderFragment()
            }
            1 -> {
                val deliverdFragment:DeliveredFragment = DeliveredFragment()
                return DeliveredFragment()
            }
            2 -> {
                 val cancelFragment = CancelledFragment()
                return CancelledFragment()
            }
            else -> return OrderFragment()
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}