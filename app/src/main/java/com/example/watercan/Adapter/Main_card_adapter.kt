package com.example.watercan.Adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Activity.OrderDetail
import com.example.watercan.Pojo.Products_Model
import com.example.watercan.R

//
//
//class Main_card_adapter(
//    product_model: List<Products_Model>,
//    mContext: Context
//) :
//    RecyclerView.Adapter<Main_card_adapter.MyViewHolder?>() {
//    private var product_model: List<Products_Model>
//
//    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
//        lateinit var product_name: TextView
//        lateinit var litres : TextView
//        lateinit var status : TextView
//        lateinit var price : TextView
//        lateinit var quantity : TextView
//
//        var badgecount: TextView? = null
//        var dashboardImage: ImageView? = null
//
//
//        init {
//            product_name = view.findViewById<View>(R.id.prod_name) as TextView
//            litres = view.findViewById<View>(R.id.litres) as TextView
//            status = view.findViewById<View>(R.id.order_status) as TextView
//            quantity = view.findViewById<View>(R.id.quantity) as TextView
//        }
//    }
//
//    var context: Context
//
//    fun UpdateAdapter(product_model: List<Products_Model>) {
//        this.product_model = product_model
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
//        val itemView: View = LayoutInflater.from(context)
//            .inflate(R.layout.main_item_card, parent, false)
//        return MyViewHolder(itemView)
//    }
//
//    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        val dashboardList: Products_Model = product_model[position]
//        holder.product_name.text = dashboardList.product_name.toString()
//        holder.litres.text = dashboardList.litres
//        holder.price.text=dashboardList.amount
//        holder.status.text = dashboardList.status
//        holder.quantity.text = dashboardList.quantity
//
//
//    }
//
//    override fun getItemCount(): Int {
//        return 20
//    }
//
//    init {
//        this.product_model = product_model
//        context = mContext
//    }
//
//
//}
//




class Main_card_adapter(
    product_model: List<Products_Model>,
    val activity: Activity
) :
    RecyclerView.Adapter<Main_card_adapter.MyViewHolder?>() {
//    private var product_model: List<Products_Model>

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var product_name: TextView
        lateinit var litres : TextView
        lateinit var status : TextView
        lateinit var price : TextView
        lateinit var quantity : TextView
        lateinit var card_view : CardView

        var badgecount: TextView? = null
        var dashboardImage: ImageView? = null


        init {
            product_name = view.findViewById<View>(R.id.prod_name) as TextView
            litres = view.findViewById<View>(R.id.litres) as TextView
            status = view.findViewById<View>(R.id.order_status) as TextView
            quantity = view.findViewById<View>(R.id.quantity) as TextView
            card_view = view.findViewById<View>(R.id.card_view) as CardView


        }

        fun bindData(activity: Activity, position: Int){
            itemView.setOnClickListener {

            }
        }
    }

//    var context: Context

//    fun UpdateAdapter(product_model: List<Products_Model>) {
//        this.product_model = product_model
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View = LayoutInflater.from(activity)
            .inflate(R.layout.main_item_card, parent, false)
//        val view = (LayoutInflater.from(parent.context).inflate(R.layout.main_item_card, parent, false))

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        val dashboardList: Products_Model = product_model[position]
//        holder.product_name.text = dashboardList.product_name.toString()
//        holder.litres.text = dashboardList.litres
//        holder.price.text=dashboardList.amount
//        holder.status.text = dashboardList.status
//        holder.quantity.text = dashboardList.quantity

        val name: String = holder.product_name.text.toString()


        holder.bindData(activity,position)
        holder.card_view.setOnClickListener {

            val intent =Intent(activity, OrderDetail::class.java)
            intent.putExtra("productname",name)
            activity.startActivity(intent)


        }



    }

    override fun getItemCount(): Int {
        return 20
    }

//    init {
//        this.product_model = product_model
//        context = mContext
//    }


}
