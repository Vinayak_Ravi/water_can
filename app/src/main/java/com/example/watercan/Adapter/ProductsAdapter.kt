package com.example.watercan.Adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.R


class ProductsAdapter(
    val activity: Activity
) :
    RecyclerView.Adapter<ProductsAdapter.MyViewHolder?>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var prod_name: TextView
        lateinit var litres : TextView
        lateinit var ruppess : TextView
        lateinit var card_view : CardView

        var badgecount: TextView? = null
        var dashboardImage: ImageView? = null


        init {
            prod_name = view.findViewById<View>(R.id.prod_name) as TextView
            litres = view.findViewById<View>(R.id.litres) as TextView
            ruppess = view.findViewById<View>(R.id.ruppess) as TextView
            card_view = view.findViewById<View>(R.id.card_view) as CardView


        }

        fun bindData(activity: Activity, position: Int){
            itemView.setOnClickListener {

            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View = LayoutInflater.from(activity)
            .inflate(R.layout.products_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {



        holder.bindData(activity,position)
//        holder.card_view.setOnClickListener {
//
//            val intent = Intent(activity, OrderDetail::class.java)
//            intent.putExtra("productname",name)
//            activity.startActivity(intent)
//
//
//        }



    }

    override fun getItemCount(): Int {
        return 20
    }



}
