package com.example.watercan.Adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Activity.OrderDetail
import com.example.watercan.R


class DeliveryBoyAdapter(
    val activity: Activity
) :
    RecyclerView.Adapter<DeliveryBoyAdapter.MyViewHolder?>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var deli_boy_name: TextView
        lateinit var deli_boy_number : TextView
        lateinit var card_view : CardView

        var badgecount: TextView? = null
        var dashboardImage: ImageView? = null


        init {
            deli_boy_name = view.findViewById<View>(R.id.deli_boy_name) as TextView
            deli_boy_number = view.findViewById<View>(R.id.deli_boy_number) as TextView
            card_view = view.findViewById<View>(R.id.card_view) as CardView


        }

        fun bindData(activity: Activity, position: Int){
            itemView.setOnClickListener {

            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View = LayoutInflater.from(activity)
            .inflate(R.layout.delivery_boy_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {



        holder.bindData(activity,position)
//        holder.card_view.setOnClickListener {
//
//            val intent = Intent(activity, OrderDetail::class.java)
//            intent.putExtra("productname",name)
//            activity.startActivity(intent)
//
//
//        }



    }

    override fun getItemCount(): Int {
        return 20
    }



}
