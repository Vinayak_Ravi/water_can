package com.example.watercan.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Adapter.CustomerAdapter
import com.example.watercan.Adapter.DeliveryBoyAdapter
import com.example.watercan.Fragment.AddCustomers
import com.example.watercan.Fragment.Add_Driver
import com.example.watercan.R
import kotlinx.android.synthetic.main.activity_customers.*
import kotlinx.android.synthetic.main.activity_delivery_boy.*
import kotlinx.android.synthetic.main.activity_delivery_boy.add
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.activity_order.back

class Customers : AppCompatActivity() {

    private var card_adapter: CustomerAdapter? = null
    lateinit var title: TextView
    lateinit var close: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customers)

        back.setOnClickListener(View.OnClickListener { finish() })

        title = findViewById(R.id.title_tool) as TextView
        close = findViewById(R.id.close) as ImageView

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)

        card_adapter = CustomerAdapter(this)

        recyclerView.adapter = card_adapter
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager

        add.setOnClickListener {

            val bottomSheet = AddCustomers()
            bottomSheet.show(supportFragmentManager, "BottomSheetEx")
            bottomSheet.setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);


        }
        search.setOnClickListener {
            search_text.setVisibility(View.VISIBLE);
            title.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            close.setVisibility(View.VISIBLE);




        }

        close.setOnClickListener {
            search_text.setVisibility(View.GONE);
            title.setVisibility(View.VISIBLE);
            close.setVisibility(View.GONE);
            search.setVisibility(View.VISIBLE);

        }

    }
}
