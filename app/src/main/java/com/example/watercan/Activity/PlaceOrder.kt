package com.example.watercan.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Adapter.ProductBuyAdapter
import com.example.watercan.Adapter.ProductsAdapter
import com.example.watercan.R
import kotlinx.android.synthetic.main.activity_order.*

class PlaceOrder : AppCompatActivity() {

    private var card_adapter: ProductBuyAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_order)

        back.setOnClickListener(View.OnClickListener { finish() })

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)

        card_adapter = ProductBuyAdapter(this)

        recyclerView.adapter = card_adapter
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager

    }
}
