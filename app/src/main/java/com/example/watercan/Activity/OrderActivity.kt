package com.example.watercan.Activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.watercan.Adapter.OrderAdapter
import com.example.watercan.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_order.*


class OrderActivity : AppCompatActivity() {

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null
    var mDoubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        viewPager = findViewById<ViewPager>(R.id.viewPager)
        back.setOnClickListener(View.OnClickListener { finish() })
        tabLayout!!.addTab(tabLayout!!.newTab().setText(R.string.order))
        tabLayout!!.addTab(tabLayout!!.newTab().setText(R.string.delivered))
        tabLayout!!.addTab(tabLayout!!.newTab().setText(R.string.cancel))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL




        val adapter = OrderAdapter(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }
    override fun onBackPressed() {
        val doubleBackToExitPressedOnce = false
        if (viewPager!!.getCurrentItem() > 0) {
            //if any tab selected instead of tab 1
            mDoubleBackToExitPressedOnce = false
        } else if (viewPager!!.getCurrentItem() === 0) {
            //if tab 1 selected
            mDoubleBackToExitPressedOnce = true
            if (mDoubleBackToExitPressedOnce) super.onBackPressed()
        }
        viewPager!!.setCurrentItem(0) //go to tab 1
    }

}



