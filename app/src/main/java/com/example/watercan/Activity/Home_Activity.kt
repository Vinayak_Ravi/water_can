package com.example.watercan

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Activity.*
import com.example.watercan.Adapter.Main_card_adapter
import com.example.watercan.Fragment.AddCustomers
import com.example.watercan.Pojo.Products_Model
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_delivery_boy.*
import kotlinx.android.synthetic.main.activity_home_.*
import kotlinx.android.synthetic.main.activity_home_.toolbar
import java.util.*

class Home_Activity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var card_adapter: Main_card_adapter? = null
    private var model: List<Products_Model> =
        ArrayList<Products_Model>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_)

        setSupportActionBar(toolbar)

        var toggleLayout = ActionBarDrawerToggle(this,drawer_layout,toolbar,R.string.Open,R.string.Close)
        drawer_layout.addDrawerListener(toggleLayout)
//        toggleLayout.isDrawerIndicatorEnabled = true

        toggleLayout.syncState()


        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.menu)
        navigationView.setNavigationItemSelectedListener(this)
        supportActionBar?.setDisplayShowTitleEnabled(false);



        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)

        card_adapter = Main_card_adapter(model,this)
        model = ArrayList<Products_Model>()

        recyclerView.adapter = card_adapter
        val mLayoutManager: RecyclerView.LayoutManager = GridLayoutManager(this, 2)
        recyclerView.layoutManager = mLayoutManager

        place_order.setOnClickListener {
            val intent = Intent(this,PlaceOrder::class.java)
            startActivity(intent)


        }
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {

        when (p0.itemId) {
            R.id.order->{
                val intent = Intent(this,OrderActivity::class.java)
                startActivity(intent)
            }
            R.id.customers -> {
                val intent = Intent(this,Customers::class.java)
                startActivity(intent)
            }
            R.id.products-> {
                val intent = Intent(this,Products::class.java)
                startActivity(intent)
            }
            R.id. delivery-> {
                val intent = Intent(this,Delivery_boy::class.java)
                startActivity(intent)
            }
            R.id.reports ->{
                val intent = Intent(this,Reports::class.java)
                startActivity(intent)
            }


        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
