package com.example.watercan

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_mobile_number_.*

class MobileNumber_Activity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_number_)


        send.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when(v?.id){

            R.id.send ->{
                //Phone Number Validation
                if (mobile_number.text.toString().trim().equals("")) {
                    mobile_number.setError("Please enter the phone number")
                    mobile_number.requestFocus()
                }else if (!isValidPhone(mobile_number.text.toString())) {
                    mobile_number.setError("Phone number is not valid")
                    mobile_number.requestFocus()
                } else if(isValidPhone(mobile_number.text.toString())){
                    val intent = Intent(this,Otp_Activity::class.java)
                    startActivity(intent)

                }


            }


        }
    }


    //Phone Number Validation
    fun isValidPhone(phone: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(phone)) {
            false
        } else {
            Patterns.PHONE.matcher(phone).matches()
        }
    }
}
