package com.example.watercan.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.watercan.R
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.activity_order.back
import kotlinx.android.synthetic.main.activity_reports.*

class Reports : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reports)

        back.setOnClickListener(View.OnClickListener { finish() })


        unreturn_can_lay.setOnClickListener {
            val intent = Intent(this,UnreturnedCanList::class.java)
            startActivity(intent)
        }

        outstanding_amount_lay.setOnClickListener {
            val intent = Intent(this,OutStanding_Amount::class.java)
            startActivity(intent)
        }

    }
}
