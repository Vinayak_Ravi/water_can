package com.example.watercan.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Adapter.UnReturnedCountAdapter
import com.example.watercan.R
import kotlinx.android.synthetic.main.activity_out_standing__amount.*

class UnreturnedCanList : AppCompatActivity() {


    private var card_adapter: UnReturnedCountAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unreturned_can_list)



        back.setOnClickListener { finish() }

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)

        card_adapter = UnReturnedCountAdapter(this)

        recyclerView.adapter = card_adapter
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager
    }
}
