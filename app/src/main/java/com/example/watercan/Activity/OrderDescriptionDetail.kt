package com.example.watercan.Activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.watercan.R
import kotlinx.android.synthetic.main.activity_order_description_detail.*

class OrderDescriptionDetail : AppCompatActivity() {

    var count = 1
    var amount = 30
    var total = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_description_detail)


        back.setOnClickListener { finish() }


        price.setText("Rs "+amount.toString()+".00")


        plus.setOnClickListener(View.OnClickListener {

            count++

            product_count.setText(count.toString())

            total = amount*count


            price.setText("Rs " + total.toString() +".00")

            quantity.setText("QUANTITY: "+count.toString())

        })


        minus.setOnClickListener(View.OnClickListener {
            if (product_count.getText().toString().trim { it <= ' ' } == "1") {
            } else {
                count = product_count.getText().toString().trim { it <= ' ' }.toInt()
                count--
                product_count.setText(count.toString())
                quantity.setText("QUANTITY: "+count.toString())

                total = total - amount


                price.setText("Rs " + total.toString() +".00")
            }
        })




    }
}
