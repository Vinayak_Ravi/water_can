package com.example.watercan.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.watercan.R
import kotlinx.android.synthetic.main.activity_order.*

class OrderDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)
        back.setOnClickListener(View.OnClickListener { finish() })

    }
}
