package com.example.watercan.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Adapter.CustomerAdapter
import com.example.watercan.Adapter.ProductsAdapter
import com.example.watercan.Fragment.AddCustomers
import com.example.watercan.Fragment.AddProduct
import com.example.watercan.R
import kotlinx.android.synthetic.main.activity_delivery_boy.*
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.activity_order.back

class Products : AppCompatActivity() {

    private var card_adapter: ProductsAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)


        back.setOnClickListener(View.OnClickListener { finish() })


        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)

        card_adapter = ProductsAdapter(this)

        recyclerView.adapter = card_adapter
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager

        add.setOnClickListener {

            val bottomSheet = AddProduct()
            bottomSheet.show(supportFragmentManager, "BottomSheetEx")
            bottomSheet.setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);


        }
    }
}
