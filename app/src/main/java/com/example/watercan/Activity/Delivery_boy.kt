package com.example.watercan.Activity

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.DialogFragment.STYLE_NORMAL
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Adapter.DeliveryBoyAdapter
import com.example.watercan.Fragment.Add_Driver
import com.example.watercan.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import kotlinx.android.synthetic.main.activity_delivery_boy.*
import kotlinx.android.synthetic.main.activity_order.back


class Delivery_boy : AppCompatActivity() {

    private var card_adapter: DeliveryBoyAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_boy)

        back.setOnClickListener(View.OnClickListener { finish() })


        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)

        card_adapter = DeliveryBoyAdapter(this)

        recyclerView.adapter = card_adapter
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager

        add.setOnClickListener {

            val bottomSheet = Add_Driver()
            bottomSheet.show(supportFragmentManager, "BottomSheetEx")
            bottomSheet.setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);


        }


    }

}
