package com.example.watercan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_address_activity.*

class Address_activity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address_activity)

        save.setOnClickListener(this)



    }

    override fun onClick(v: View?) {

        when(v?.id){
            R.id.save->{
                var intent =Intent(this,Home_Activity::class.java)
                startActivity(intent)
            }
        }
    }
}
