package com.example.watercan

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_otp_.*


class Otp_Activity : AppCompatActivity(), View.OnClickListener {
    var sb = StringBuilder()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_)

        //Verify Button
        verify.setOnClickListener(this)

        //Otp Texts
        otp_one.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub
                if (sb.length == 0 && otp_one.length() == 1) {
                    sb.append(s)
                    otp_one.clearFocus()
                    otp_two.requestFocus()
                    otp_two.isCursorVisible = true
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?, start: Int, count: Int,
                after: Int
            ) {
                if (sb.length == 1) {
                    sb.deleteCharAt(0)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (sb.length == 0) {
                    otp_one.requestFocus()
                }
            }
        })

        otp_two.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub
                if (sb.length == 0 && otp_two.length() == 1) {
                    sb.append(s)
                    otp_two.clearFocus()
                    otp_three.requestFocus()
                    otp_three.isCursorVisible = true
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?, start: Int, count: Int,
                after: Int
            ) {
                if (sb.length == 1) {
                    sb.deleteCharAt(0)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (sb.length == 0) {
                    otp_two.requestFocus()
                }
            }
        })

        otp_three.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub
                if (sb.length == 0 && otp_two.length() == 1) {
                    sb.append(s)
                    otp_three.clearFocus()
                    otp_four.requestFocus()
                    otp_four.isCursorVisible = true
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?, start: Int, count: Int,
                after: Int
            ) {
                if (sb.length == 1) {
                    sb.deleteCharAt(0)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (sb.length == 0) {
                    otp_three.requestFocus()
                }
            }
        })


    }


    //ClickListener
    override fun onClick(v: View?) {
        when(v?.id){

            R.id.verify ->{
                if (!otp_one.text.toString().trim().equals("") && !otp_two.text.toString().trim().equals("") && !otp_three.text.toString().trim().equals("") && !otp_four.text.toString().trim().equals("")){
                    val intent = Intent(this,Address_activity::class.java)
                    startActivity(intent)
                }else{
                    Toast.makeText(this, "Please enter the OTP", Toast.LENGTH_SHORT).show();

                }

            }
        }
    }

}
