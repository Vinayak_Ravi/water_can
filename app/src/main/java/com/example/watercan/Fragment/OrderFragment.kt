package com.example.watercan.Fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.watercan.Adapter.Main_card_adapter
import com.example.watercan.Pojo.Products_Model
import com.example.watercan.R
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class OrderFragment : Fragment() {
    private var card_adapter: Main_card_adapter? = null
    private var model: List<Products_Model> =
        ArrayList<Products_Model>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(
            R.layout.fragment_order, container,
            false
        )

        var recyclerView = view.findViewById(R.id.recyclerview) as? RecyclerView


        card_adapter = Main_card_adapter(model, context as Activity)
        model = ArrayList<Products_Model>()

        recyclerView?.adapter = card_adapter
        val mLayoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, 2)
        recyclerView?.layoutManager = mLayoutManager

        return view
    }

}
