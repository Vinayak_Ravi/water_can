package com.example.watercan.Fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.example.watercan.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_add__driver.*


/**
 * A simple [Fragment] subclass.
 */
class Add_Driver : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);

        // Inflate the layout for this fragment
        val view: View = inflater.inflate(
            R.layout.fragment_add__driver, container,
            false

        )
//        getDialog()!!.getWindow()!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);
        val close = view.findViewById<ImageView>(R.id.close)

        close.setOnClickListener {
            dismiss()
        }

        return view
    }



}
